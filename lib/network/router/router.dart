enum FusionAuthRouter {
  login,
  refreshToken,
  registration,
}