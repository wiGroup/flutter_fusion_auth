
import '../network.dart';

extension FusionAuthRouterPath on FusionAuthRouter {
  String get host => FusionAuthNetworkLayer().baseUrl;

  // ignore: missing_return
  String get path {
    switch (this) {
      case FusionAuthRouter.login:
        return 'api/login';
      case FusionAuthRouter.refreshToken:
        return 'api/jwt/refresh';
      case FusionAuthRouter.registration:
        return 'api/user/registration';
    }
  }
}