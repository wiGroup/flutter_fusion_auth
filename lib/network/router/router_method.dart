
import 'package:flutter_abstract_network_payload/network/network.dart';

import '../network.dart';

extension FusionAuthRouterMethod on FusionAuthRouter {
  // ignore: missing_return
  HttpMethod get method {
    switch (this) {
      case FusionAuthRouter.login:
      case FusionAuthRouter.refreshToken:
      case FusionAuthRouter.registration:
        return HttpMethod.post;
    }
  }
}
