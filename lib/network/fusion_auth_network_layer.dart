import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_abstract_network_payload/network/network.dart';
import 'package:http/http.dart';

import 'network.dart';

@protected
class FusionAuthNetworkLayer {
  static FusionAuthNetworkLayer _instance;

  FusionAuthNetworkLayer._internal();

  factory FusionAuthNetworkLayer() {
    _instance ??= FusionAuthNetworkLayer._internal();

    return _instance;
  }

  String baseUrl;
  String errorMessage;

  init({String baseUrl, String errorMessage}) {
    this.baseUrl = baseUrl;
    this.errorMessage = errorMessage;
  }

  Future<T> request<T>(
      FusionAuthRouter router, NetworkFusionAuthPayLoad data) async {
    final response = await _requestContentResponse<T>(router, data);
    String content = await response.stream.bytesToString();

    try {
      final decodedContent = content.isEmpty ? null : json.decode(content);
      final decodedResponse =
              data.responseWithStatus(decodedContent, response.statusCode);
      return Future.value(decodedResponse);
    } catch (e) {
      return Future.error(errorMessage);
    }
  }

  Future<StreamedResponse> _requestContentResponse<T>(
      FusionAuthRouter router, NetworkFusionAuthPayLoad data) async {
    final _client = Client();

    Map<String, String> additionalHeaders;
    final request =
        Request(router.method.value, _url(router, data?.queryParameters));

    if (data != null) {
      request.body = json.encode(data.request());
      additionalHeaders = data.additionalHeaders;
    }

    request.headers.addAll(_headers(router, additionalHeaders));

    try {
      return await _client.send(request);
    } catch (error) {
      return Future.error(errorMessage != null || errorMessage.isNotEmpty
          ? errorMessage
          : error);
    }
  }

  Uri _url(FusionAuthRouter router, Map<String, String> queryParameters) {
    return Uri.https(router.host, router.path, queryParameters);
  }

  Map<String, String> _headers(
      FusionAuthRouter router, Map<String, String> additionalHeaders) {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      ...additionalHeaders ?? {}
    };

    return headers;
  }
}
