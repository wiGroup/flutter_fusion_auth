library flutter_fusion_auth;

import 'package:flutter_fusion_auth/models/refresh_token.dart';

import './models/models.dart';
import './network/network.dart';

class UserRegistrationService {
  static Future registration(
      User user, String applicationId, String authorizationKey) async {
    final router = FusionAuthRouter.registration;
    final registration = Registration(applicationId: applicationId);

    final payload = UserRegistrationPayLoad();
    payload.registration = registration;
    payload.user = user;
    payload.authorizationKey = authorizationKey;

    return FusionAuthNetworkLayer().request(router, payload).then((response) {
      return Future.value(response);
    });
  }

  static Future login(String loginId, String password, String applicationId,
      String authorizationKey) async {
    final router = FusionAuthRouter.login;
    final payload = LoginPayLoad();
    payload.loginId = loginId;
    payload.password = password;
    payload.applicationId = applicationId;
    payload.authorizationKey = authorizationKey;

    return FusionAuthNetworkLayer().request(router, payload).then((response) {
      return Future.value(response);
    });
  }

  static Future<String> refreshToken(
      String tenantId, String refreshToken) async {
    final payload = RefreshTokenPayload(tenantId, refreshToken);

    return FusionAuthNetworkLayer()
        .request(FusionAuthRouter.refreshToken, payload);
  }
}
