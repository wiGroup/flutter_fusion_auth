import 'package:flutter_abstract_network_payload/network/http_request_algorithm.dart';
import 'package:flutter_fusion_auth/network/network.dart';

import 'models.dart';

class LoginPayLoad extends NetworkFusionAuthPayLoad {
  String loginId;
  String password;
  String applicationId;
  String authorizationKey;

  @override
  Map<String, String> get additionalHeaders =>
      {'Authorization': authorizationKey};

  @override
  Map<String, String> get queryParameters => null;

  @override
  Map<String, dynamic> request() => {
        'loginId': loginId,
        'password': password,
        'applicationId': applicationId,
      };

  @override
  LoginResponse response(Map<String, dynamic> json) => null;

  @override
  Map<String, String> get pathParameters => null;

  @override
  LoginResponse responseWithStatus(Map<String, dynamic> json, int statusCode) =>
      LoginResponse.fromJson(json, statusCode);

  @override
  EHttpRequestAlgorithm get requestAlgorithm => EHttpRequestAlgorithm.multiple;
}

class LoginResponse {
  User user;
  String token;
  String refreshToken;
  EFusionAuthResponseCode responseCode;

  LoginResponse({
    this.user,
    this.token,
    this.refreshToken,
    this.responseCode,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json, int statusCode) {
    User user;
    String token;
    String refreshToken;

    if (json != null && json.isNotEmpty) {
      token = json['token'] ?? "";

      if (json['user'] != null) user = User.fromJson(json['user']);
      if (json['refreshToken'] != null) refreshToken = json['refreshToken'];
    }

    return LoginResponse(
      user: user,
      token: token,
      refreshToken: refreshToken,
      responseCode: EFusionAuthResponseCodeExtension.fromInt(statusCode),
    );
  }
}
