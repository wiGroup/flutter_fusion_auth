enum EFusionAuthResponseCode {
  success,
  duplicateInfo,
  notFound,
  loginPrevention,
  loginExpired,
  accountLocked,
  internalError,
  exceptionError,
}

extension EFusionAuthResponseCodeExtension on EFusionAuthResponseCode {
  static const enumValues = {
    EFusionAuthResponseCode.success: 200,
    EFusionAuthResponseCode.duplicateInfo: 400,
    EFusionAuthResponseCode.notFound: 404,
    EFusionAuthResponseCode.loginPrevention: 409,
    EFusionAuthResponseCode.loginExpired: 410,
    EFusionAuthResponseCode.accountLocked: 423,
    EFusionAuthResponseCode.internalError: 500,
    EFusionAuthResponseCode.exceptionError: 503,
  };

  static EFusionAuthResponseCode fromInt(int value) {
    switch (value) {
      case 200:
        return EFusionAuthResponseCode.success;
      case 202:
        return EFusionAuthResponseCode.success;
      case 203:
        return EFusionAuthResponseCode.success;
      case 212:
        return EFusionAuthResponseCode.success;
      case 242:
        return EFusionAuthResponseCode.success;
      case 400:
        return EFusionAuthResponseCode.duplicateInfo;
      case 409:
        return EFusionAuthResponseCode.loginPrevention;
      case 410:
        return EFusionAuthResponseCode.loginExpired;
      case 423:
        return EFusionAuthResponseCode.accountLocked;
      case 500:
        return EFusionAuthResponseCode.internalError;
      case 503:
        return EFusionAuthResponseCode.exceptionError;
      default:
        return EFusionAuthResponseCode.notFound;
    }
  }

  int get value => enumValues[this];
}
