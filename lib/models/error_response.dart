import 'package:flutter/material.dart';

@protected
class ErrorResponse {
  List<Error> emailErrors;
  List<Error> usernameErrors;

  ErrorResponse({this.emailErrors, this.usernameErrors});

  factory ErrorResponse.fromJson(Map<String, dynamic> json) {
    final jsonEmailErrors = json['user.email'] ?? [];
    final emailErrors =
        List<Error>.from(jsonEmailErrors.map((item) => Error.fromJson(item)));

    final jsonUsernameErrors = json['user.username'] ?? [];
    final usernameErrors = List<Error>.from(
        jsonUsernameErrors.map((item) => Error.fromJson(item)));

    return ErrorResponse(
      emailErrors: emailErrors,
      usernameErrors: usernameErrors,
    );
  }
}

class Error {
  String code;
  String message;

  Error({this.code, this.message});

  factory Error.fromJson(Map<String, dynamic> json) {
    return Error(
      code: json['code'],
      message: json['message'],
    );
  }
}
