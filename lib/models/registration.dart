import 'package:flutter_abstract_network_payload/network/http_request_algorithm.dart';
import 'package:flutter_fusion_auth/network/network.dart';

import 'models.dart';

class Registration {
  String applicationId;
  String id;
  bool verified;

  Registration({
    this.applicationId,
    this.verified,
    this.id,
  });

  factory Registration.fromJson(Map<String, dynamic> json) {
    return Registration(
      applicationId: json['applicationId'],
      id: json['id'],
      verified: json['verified'],
    );
  }

  Map<String, dynamic> toJson() => {
        'applicationId': applicationId,
      };
}

class UserRegistrationPayLoad extends NetworkFusionAuthPayLoad {
  Registration registration;
  User user;
  String authorizationKey;

  @override
  Map<String, String> get additionalHeaders =>
      {'Authorization': authorizationKey};

  @override
  Map<String, String> get queryParameters => null;

  @override
  Map<String, dynamic> request() => {
        'registration': registration.toJson(),
        'user': user.toJson(),
      };

  @override
  Map<String, String> get pathParameters => null;

  @override
  response(Map<String, dynamic> json) => null;

  @override
  UserRegistrationResponse responseWithStatus(
          Map<String, dynamic> json, int statusCode) =>
      UserRegistrationResponse.fromJson(json, statusCode);

  @override
  EHttpRequestAlgorithm get requestAlgorithm => EHttpRequestAlgorithm.multiple;
}

class UserRegistrationResponse {
  Registration registration;
  User user;
  ErrorResponse errorResponse;
  EFusionAuthResponseCode responseCode;

  UserRegistrationResponse({
    this.registration,
    this.user,
    this.errorResponse,
    this.responseCode,
  });

  factory UserRegistrationResponse.fromJson(
      Map<String, dynamic> json, int statusCode) {
    Registration registration;
    User user;
    ErrorResponse errorResponse;

    if (json != null && json.isNotEmpty) {
      final jsonRegistration = json['registration'] ?? [];
      if (jsonRegistration.isNotEmpty)
        registration = Registration.fromJson(jsonRegistration);

      final jsonUser = json['user'] ?? [];
      if (jsonUser.isNotEmpty) user = User.fromJson(jsonUser);

      final jsonError = json['fieldErrors'] ?? [];
      if (jsonError.isNotEmpty)
        errorResponse = ErrorResponse.fromJson(jsonError);
    }

    return UserRegistrationResponse(
      registration: registration,
      user: user,
      errorResponse: errorResponse,
      responseCode: EFusionAuthResponseCodeExtension.fromInt(statusCode),
    );
  }
}
