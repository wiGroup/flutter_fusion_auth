class User {
  bool active;
  String id;
  String firstName;
  String lastName;
  String mobilePhone;
  String email;
  String username;
  String password;
  bool verified;
  UserMetaData userMetaData;

  User({
    this.active,
    this.id,
    this.firstName,
    this.lastName,
    this.mobilePhone,
    this.email,
    this.username,
    this.password,
    this.verified,
    this.userMetaData,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    UserMetaData data;
    final jsonData = json['data'];
    if (jsonData != null)
      data = UserMetaData.fromJson(json['data']);

    return User(
      active: json['active'],
      id: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      mobilePhone: json['mobilePhone'],
      email: json['email'],
      username: json['username'],
      password: json['password'],
      verified: json['verified'],
      userMetaData: data,
    );
  }

  Map<String, dynamic> toJson() => {
        'firstName': firstName,
        'lastName': lastName,
        'mobilePhone': mobilePhone,
        'email': email,
        'username': username,
        'password': password,
      };
}

class UserMetaData {
  String subscriberId;
  String subscriberState;
  String system;

  UserMetaData({
    this.subscriberId,
    this.subscriberState,
    this.system,
  });

  factory UserMetaData.fromJson(Map<String, dynamic> json) {
    var subscriberId;
    final jsonSubscriberId = json['subscriberId'];
    if (jsonSubscriberId is String)
      subscriberId =  jsonSubscriberId;
    else
      subscriberId = jsonSubscriberId.toString();

    return UserMetaData(
      subscriberId: subscriberId,
      subscriberState: json['subscriberState'],
      system: json['system'],
    );
  }
}
