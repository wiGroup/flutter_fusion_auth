import 'package:flutter_abstract_network_payload/network/network.dart';

import '../network/network.dart';

class RefreshTokenPayload extends NetworkFusionAuthPayLoad {
  String tenantId;
  String refreshToken;

  RefreshTokenPayload(this.tenantId, this.refreshToken);

  @override
  Map<String, String> get additionalHeaders =>
      {'X-FusionAuth-TenantId': tenantId};

  @override
  Map<String, String> get queryParameters => null;

  @override
  Map<String, dynamic> request() => {
        'refreshToken': refreshToken,
      };

  @override
  String response(Map<String, dynamic> json) => null;

  @override
  Map<String, String> get pathParameters => null;

  @override
  String responseWithStatus(Map<String, dynamic> json, int statusCode) =>
      json['token'];

  @override
  EHttpRequestAlgorithm get requestAlgorithm => EHttpRequestAlgorithm.replace;
}
